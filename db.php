<?php

class DB {
    /**
     * @return PDO
     */
	public static function get_connection (){
	  	$opt = [
	        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
	        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
	        PDO::ATTR_EMULATE_PREPARES   => false,
		];
		$config = require_once ($_SERVER['DOCUMENT_ROOT'].'/config/db_config.php');
		$dbh = new PDO("mysql:host=".$config['host'].";dbname=".$config['db_name'], $config['user'], $config['password'], $opt);

		return $dbh;
	}
}