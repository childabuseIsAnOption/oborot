<?php 
if(!empty($_GET['user_id'])&&(!empty($_GET['quantity']))){
	require $_SERVER['DOCUMENT_ROOT']."/faker/vendor/autoload.php";
	require $_SERVER['DOCUMENT_ROOT']."/db.php";
	$faker = Faker\Factory::create('ru_RU');
	$sql = "INSERT INTO `note` (user_id, title, content, date_create) VALUES ";
	for ($i = 0; $i< intval($_GET['quantity']); $i++){
		$content = $faker->realText(350);
		$date_create = $faker->date($format = 'Y-m-d H:i:s', $max = 'now');
		$date_create = strtotime ($date_create);
		$title = substr($content, 0, 50);
		$sql = $sql. "('{$_GET['user_id']}', '{$title}', '{$content}', '{$date_create}'), ";
	}
	$sql = trim($sql, ', ');
	$db = DB::get_connection();
	$result = $db->query($sql);
	$last_id = $db->lastInsertId();
	if ($last_id > 0 ){
		$result = [
			'status' => 1,
			'message' => 'добавлено'
		];
	} else {
		$result = [
			'status' => 0,
			'message' => 'ошибка добавления'
		];
	}

} else {
	$result = [
		'status' => 0,
		'message' => 'Не переданы данные',
	];
}

header("Content-type:application/json");
echo json_encode($result);