<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>

<form method="GET">
<label for="user">выберите пользователя, которому нужно добавить заметки:</label><br>
<select id="user" name="user">
	<option selected disabled>
	<?php while ($row = $result->fetch()){
		echo "<option value={$row['id']}>{$row['name']}</option>";
	}
	?>
</select> <br>
<label for="quantity">количество записей</label><br>
<input type="number" name="quantity" id='quantity'><br>
<input type="submit" value="add" id="submit">
</form>
<a href="/">вернуться</a>
<div id="result" style="position: relative; top:50%;left:50%;font-size: 20px;color:red"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function (){
		$('#submit').on('click', function (e){
			e.preventDefault();
			var amount, id;
			amount = $('#quantity').val();
			id = $('#user').val();
			$.ajax ({
				type: "GET",
				url: '/add_notes_ajax.php?quantity='+amount+'&user_id='+id,
				success: function (data){
					console.log(data);
					$('#result').text(data.message);
				}
			})
			$('#result').fadeIn().delay(2000).fadeOut();
		})
	});
</script>
</body>
</html>