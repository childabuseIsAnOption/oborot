<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<select id="user" name="user">
	<option selected disabled>
	<?php while ($row = $result->fetch()){
		echo "<option value={$row['id']}>{$row['name']}</option>";
	}
	?>
</select> <br>
<div id="container">
	<div class="content"></div>
</div>
<a href="/">Вернуться</a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	$(document).ready(function (){
		$('#user').on ('change',function(){
			$.ajax ({
				type: "GET",
				url: '/view_notes_ajax.php?user_id='+$(this).val(),
				success: function(data){
					$('.content').remove();
					$('#container').append(data);
					console.log(data);
				}
			});
		})
	});
</script>
</body>
</html>